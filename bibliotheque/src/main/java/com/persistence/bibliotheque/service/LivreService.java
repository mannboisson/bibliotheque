package com.persistence.bibliotheque.service;

import com.persistence.bibliotheque.bo.LivreBo;
import com.persistence.bibliotheque.entity.Livre;
import com.persistence.bibliotheque.repository.LivreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class LivreService {

    private final LivreRepository livreRepository;

    @Autowired
    public LivreService(LivreRepository livreRepository) {
        this.livreRepository = livreRepository;
    }

    private Livre livreBoToEntity(LivreBo livreBo) {
        Livre livre = new Livre();
        livre.setId(livreBo.getId());
        livre.setTitre(livreBo.getTitre());
        livre.setDateParution(livreBo.getDateDeParution());
        livre.setNombrePages(livreBo.getNombreDePages());
        livre.setAuteur(livreBo.getAuteur());
        livre.setCategorie(livreBo.getCategorie());
        return livre;
    }



    private LivreBo entityToLivreBo(Livre livre) {
        return new LivreBo(
                livre.getId(),
                livre.getTitre(),
                livre.getDateParution(),
                livre.getNombrePages(),
                livre.getAuteur(),
                livre.getCategorie()
        );
    }


    public LivreBo createLivre(LivreBo livreBo) {
        Livre nouveauLivre = livreBoToEntity(livreBo);
        Livre livreCree = livreRepository.save(nouveauLivre);
        return entityToLivreBo(livreCree);
    }

    public LivreBo findLivreById(Long id) {
        Optional<Livre> optionalLivre = livreRepository.findById(id);
        return optionalLivre.map(this::entityToLivreBo).orElse(null);
    }

    public LivreBo updateLivre(LivreBo livreBo) {
        Optional<Livre> optionalLivre = livreRepository.findById(livreBo.getId());
        if (optionalLivre.isPresent()) {
            Livre livre = optionalLivre.get();
            livre.setTitre(livreBo.getTitre());
            livre.setDateParution(livreBo.getDateDeParution());
            livre.setNombrePages(livreBo.getNombreDePages());
            livre.setAuteur(livreBo.getAuteur());
            livre.setCategorie(livreBo.getCategorie());
            livreRepository.save(livre);
            return entityToLivreBo(livre);
        }
        return null;
    }

}


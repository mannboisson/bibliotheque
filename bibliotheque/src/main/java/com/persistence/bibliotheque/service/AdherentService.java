package com.persistence.bibliotheque.service;

import com.persistence.bibliotheque.bo.AdherentBo;
import com.persistence.bibliotheque.entity.Adherent;
import com.persistence.bibliotheque.repository.AdherentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AdherentService {

    private final AdherentRepository adherentRepository;

    @Autowired
    public AdherentService(AdherentRepository adherentRepository) {
        this.adherentRepository = adherentRepository;
    }

    private Adherent adherentBoToEntity(AdherentBo adherentBo) {
        return new Adherent(adherentBo.getId(), adherentBo.getNom(), adherentBo.getPrenom(), adherentBo.getEmail(), adherentBo.getDateInscription());
    }

    private AdherentBo entityToAdherentBo(Adherent adherent) {
        return new AdherentBo(adherent.getId(), adherent.getNom(), adherent.getPrenom(), adherent.getEmail(), adherent.getDateInscription());
    }

    public AdherentBo createAdherent(AdherentBo adherentBo) {
        Adherent nouvelAdherent = adherentBoToEntity(adherentBo);
        Adherent adherentCree = adherentRepository.save(nouvelAdherent);
        return entityToAdherentBo(adherentCree);
    }

    public AdherentBo findAdherentById(Long id) {
        Optional<Adherent> optionalAdherent = adherentRepository.findById(id);
        return optionalAdherent.map(this::entityToAdherentBo).orElse(null);
    }

    public AdherentBo updateAdherent(AdherentBo adherentBo) {
        Optional<Adherent> optionalAdherent = adherentRepository.findById(adherentBo.getId());
        if (optionalAdherent.isPresent()) {
            Adherent adherent = optionalAdherent.get();
            adherent.setNom(adherentBo.getNom());
            adherent.setPrenom(adherentBo.getPrenom());
            adherent.setEmail(adherentBo.getEmail());
            adherent.setDateInscription(adherentBo.getDateInscription());
            adherentRepository.save(adherent);
            return entityToAdherentBo(adherent);
        }
        return null;
    }
}

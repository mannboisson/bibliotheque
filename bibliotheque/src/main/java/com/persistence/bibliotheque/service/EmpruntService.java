package com.persistence.bibliotheque.service;

import com.persistence.bibliotheque.bo.AdherentBo;
import com.persistence.bibliotheque.bo.EmpruntBo;
import com.persistence.bibliotheque.bo.LivreBo;
import com.persistence.bibliotheque.entity.Adherent;
import com.persistence.bibliotheque.entity.Emprunt;
import com.persistence.bibliotheque.entity.Livre;
import com.persistence.bibliotheque.repository.EmpruntRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class EmpruntService {

    private final EmpruntRepository empruntRepository;

    @Autowired
    public EmpruntService(EmpruntRepository empruntRepository) {
        this.empruntRepository = empruntRepository;
    }

    private Emprunt empruntBoToEntity(EmpruntBo empruntBo) {
        Emprunt emprunt = new Emprunt();
        emprunt.setId(empruntBo.getId());
        emprunt.setDateEmprunt(empruntBo.getDateEmprunt());
        emprunt.setDateFinPrevue(empruntBo.getDateFinPrevue());
        emprunt.setDateRetour(empruntBo.getDateRetour());

        Adherent adherent = new Adherent();
        adherent.setId(empruntBo.getAdherent().getId());

        Livre livre = new Livre();
        livre.setId(empruntBo.getLivre().getId());

        emprunt.setAdherent(adherent);
        emprunt.setLivre(livre);

        return emprunt;
    }


    private EmpruntBo entityToEmpruntBo(Emprunt emprunt) {
        return new EmpruntBo(emprunt.getId(), emprunt.getDateEmprunt(), emprunt.getDateFinPrevue(), emprunt.getDateRetour(), null, null);
    }



    public EmpruntBo createEmprunt(EmpruntBo empruntBo) {
        Emprunt nouvelEmprunt = empruntBoToEntity(empruntBo);
        Emprunt empruntCree = empruntRepository.save(nouvelEmprunt);
        return entityToEmpruntBo(empruntCree);
    }

    public EmpruntBo findEmpruntById(Long id) {
        Optional<Emprunt> optionalEmprunt = empruntRepository.findById(id);
        return optionalEmprunt.map(this::entityToEmpruntBo).orElse(null);
    }

    public EmpruntBo updateEmprunt(EmpruntBo empruntBo) {
        Optional<Emprunt> optionalEmprunt = empruntRepository.findById(empruntBo.getId());
        if (optionalEmprunt.isPresent()) {
            Emprunt emprunt = optionalEmprunt.get();
            emprunt.setDateEmprunt(empruntBo.getDateEmprunt());
            emprunt.setDateFinPrevue(empruntBo.getDateFinPrevue());
            emprunt.setDateRetour(empruntBo.getDateRetour());
            empruntRepository.save(emprunt);
            return entityToEmpruntBo(emprunt);
        }
        return null;
    }
}

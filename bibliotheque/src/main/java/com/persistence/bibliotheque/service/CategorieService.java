package com.persistence.bibliotheque.service;

import com.persistence.bibliotheque.bo.CategorieBo;
import com.persistence.bibliotheque.entity.Categorie;
import com.persistence.bibliotheque.repository.CategorieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CategorieService {

    private final CategorieRepository categorieRepository;

    @Autowired
    public CategorieService(CategorieRepository categorieRepository) {
        this.categorieRepository = categorieRepository;
    }

    private Categorie categorieBoToEntity(CategorieBo categorieBo) {
        return new Categorie(categorieBo.getId(), categorieBo.getNom());
    }

    private CategorieBo entityToCategorieBo(Categorie categorie) {
        return new CategorieBo(categorie.getId(), categorie.getNom());
    }

    public CategorieBo createCategorie(CategorieBo categorieBo) {
        Categorie nouvelleCategorie = categorieBoToEntity(categorieBo);
        Categorie categorieCree = categorieRepository.save(nouvelleCategorie);
        return entityToCategorieBo(categorieCree);
    }

    public CategorieBo findCategorieById(Long id) {
        Optional<Categorie> optionalCategorie = categorieRepository.findById(id);
        return optionalCategorie.map(this::entityToCategorieBo).orElse(null);
    }

    public CategorieBo updateCategorie(CategorieBo categorieBo) {
        Optional<Categorie> optionalCategorie = categorieRepository.findById(categorieBo.getId());
        if (optionalCategorie.isPresent()) {
            Categorie categorie = optionalCategorie.get();
            categorie.setNom(categorieBo.getNom());
            categorieRepository.save(categorie);
            return entityToCategorieBo(categorie);
        }
        return null;
    }
}

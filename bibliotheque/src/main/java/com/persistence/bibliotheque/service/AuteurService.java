package com.persistence.bibliotheque.service;

import com.persistence.bibliotheque.bo.AuteurBo;
import com.persistence.bibliotheque.entity.Auteur;
import com.persistence.bibliotheque.repository.AuteurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AuteurService {

    private final AuteurRepository auteurRepository;

    @Autowired
    public AuteurService(AuteurRepository auteurRepository) {
        this.auteurRepository = auteurRepository;
    }

    private Auteur auteurBoToEntity(AuteurBo auteurBo) {
        return new Auteur(auteurBo.getId(), auteurBo.getNom(), auteurBo.getPrenom());
    }

    private AuteurBo entityToAuteurBo(Auteur auteur) {
        return new AuteurBo(auteur.getId(), auteur.getNom(), auteur.getPrenom());
    }

    public AuteurBo createAuteur(AuteurBo auteurBo) {
        Auteur nouvelAuteur = auteurBoToEntity(auteurBo);
        Auteur auteurCree = auteurRepository.save(nouvelAuteur);
        return entityToAuteurBo(auteurCree);
    }

    public AuteurBo findAuteurById(Long id) {
        Optional<Auteur> optionalAuteur = auteurRepository.findById(id);
        return optionalAuteur.map(this::entityToAuteurBo).orElse(null);
    }

    public AuteurBo updateAuteur(AuteurBo auteurBo) {
        Optional<Auteur> optionalAuteur = auteurRepository.findById(auteurBo.getId());
        if (optionalAuteur.isPresent()) {
            Auteur auteur = optionalAuteur.get();
            auteur.setNom(auteurBo.getNom());
            auteur.setPrenom(auteurBo.getPrenom());
            auteurRepository.save(auteur);
            return entityToAuteurBo(auteur);
        }
        return null;
    }
}

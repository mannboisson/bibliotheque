package com.persistence.bibliotheque.controller;

import com.persistence.bibliotheque.bo.AuteurBo;
import com.persistence.bibliotheque.entity.Auteur;
import com.persistence.bibliotheque.repository.AuteurRepository;
import com.persistence.bibliotheque.service.AuteurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/auteurs")
public class AuteurController {

    private final AuteurRepository auteurRepository;
    private final AuteurService auteurService;

    @Autowired
    public AuteurController(AuteurRepository auteurRepository, AuteurService auteurService) {
        this.auteurRepository = auteurRepository;
        this.auteurService = auteurService;
    }

    @PostMapping
    public ResponseEntity<AuteurBo> createAuteur(@RequestBody AuteurBo auteurBo) {
        AuteurBo createdAuteur = auteurService.createAuteur(auteurBo);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdAuteur);
    }

    @GetMapping("/{id}")
    public ResponseEntity<AuteurBo> findAuteurById(@PathVariable("id") Long id) {
        AuteurBo auteur = auteurService.findAuteurById(id);
        if (auteur != null) {
            return ResponseEntity.ok(auteur);
        }
        return ResponseEntity.notFound().build();
    }

    @PutMapping("/{id}/modifier")
    public ResponseEntity<AuteurBo> updateAuteur(@PathVariable("id") Long id, @RequestBody AuteurBo auteurBo) {
        auteurBo.setId(id);
        AuteurBo updatedAuteur = auteurService.updateAuteur(auteurBo);
        if (updatedAuteur != null) {
            return ResponseEntity.ok(updatedAuteur);
        }
        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/{id}/delete")
    public ResponseEntity<Void> deleteAuteurById(@PathVariable("id") Long id) {
        auteurRepository.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping
    public ResponseEntity<List<AuteurBo>> getAllAuteurs() {
        List<AuteurBo> auteurs = auteurRepository.findAllAuteurs();
        return ResponseEntity.ok(auteurs);
    }
}

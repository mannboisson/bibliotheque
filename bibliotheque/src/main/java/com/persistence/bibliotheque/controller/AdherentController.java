package com.persistence.bibliotheque.controller;

import com.persistence.bibliotheque.bo.AdherentBo;
import com.persistence.bibliotheque.entity.Adherent;
import com.persistence.bibliotheque.repository.AdherentRepository;
import com.persistence.bibliotheque.service.AdherentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/adherents")
public class AdherentController {

    private final AdherentRepository adherentRepository;
    private final AdherentService adherentService;

    @Autowired
    public AdherentController(AdherentRepository adherentRepository, AdherentService adherentService) {
        this.adherentRepository = adherentRepository;
        this.adherentService = adherentService;
    }

    @PostMapping
    public ResponseEntity<AdherentBo> createAdherent(@RequestBody AdherentBo adherentBo) {
        AdherentBo createdAdherent = adherentService.createAdherent(adherentBo);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdAdherent);
    }

    @GetMapping("/{id}")
    public ResponseEntity<AdherentBo> findAdherentById(@PathVariable("id") Long id) {
        AdherentBo adherent = adherentService.findAdherentById(id);
        if (adherent != null) {
            return ResponseEntity.ok(adherent);
        }
        return ResponseEntity.notFound().build();
    }

    @PutMapping("/{id}/modifier")
    public ResponseEntity<AdherentBo> updateAdherent(@PathVariable("id") Long id, @RequestBody AdherentBo adherentBo) {
        adherentBo.setId(id);
        AdherentBo updatedAdherent = adherentService.updateAdherent(adherentBo);
        if (updatedAdherent != null) {
            return ResponseEntity.ok(updatedAdherent);
        }
        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/{id}/delete")
    public ResponseEntity<Void> deleteAdherentById(@PathVariable("id") Long id) {
        adherentRepository.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping
    public ResponseEntity<List<Adherent>> getAllAdherents() {
        List<Adherent> adherents = adherentRepository.findAllAdherents();
        return ResponseEntity.ok(adherents);
    }
}

package com.persistence.bibliotheque.controller;

import com.persistence.bibliotheque.bo.EmpruntBo;
import com.persistence.bibliotheque.entity.Emprunt;
import com.persistence.bibliotheque.repository.EmpruntRepository;
import com.persistence.bibliotheque.service.EmpruntService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/emprunts")
public class EmpruntController {

    private final EmpruntRepository empruntRepository;
    private final EmpruntService empruntService;

    @Autowired
    public EmpruntController(EmpruntRepository empruntRepository, EmpruntService empruntService) {
        this.empruntRepository = empruntRepository;
        this.empruntService = empruntService;
    }

    @PostMapping
    public ResponseEntity<EmpruntBo> createEmprunt(@RequestBody EmpruntBo empruntBo) {
        EmpruntBo createdEmprunt = empruntService.createEmprunt(empruntBo);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdEmprunt);
    }

    @GetMapping("/{id}")
    public ResponseEntity<EmpruntBo> findEmpruntById(@PathVariable("id") Long id) {
        EmpruntBo emprunt = empruntService.findEmpruntById(id);
        if (emprunt != null) {
            return ResponseEntity.ok(emprunt);
        }
        return ResponseEntity.notFound().build();
    }

    @PutMapping("/{id}/modifier")
    public ResponseEntity<EmpruntBo> updateEmprunt(@PathVariable("id") Long id, @RequestBody EmpruntBo empruntBo) {
        empruntBo.setId(id);
        EmpruntBo updatedEmprunt = empruntService.updateEmprunt(empruntBo);
        if (updatedEmprunt != null) {
            return ResponseEntity.ok(updatedEmprunt);
        }
        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/{id}/delete")
    public ResponseEntity<Void> deleteEmpruntById(@PathVariable("id") Long id) {
        empruntRepository.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping
    public ResponseEntity<List<EmpruntBo>> getAllEmprunts() {
        List<EmpruntBo> emprunts = empruntRepository.findAllEmprunts();
        return ResponseEntity.ok(emprunts);
    }
}

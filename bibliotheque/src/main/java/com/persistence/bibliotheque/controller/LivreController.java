package com.persistence.bibliotheque.controller;

import com.persistence.bibliotheque.bo.LivreBo;
import com.persistence.bibliotheque.entity.Emprunt;
import com.persistence.bibliotheque.entity.Livre;
import com.persistence.bibliotheque.repository.EmpruntRepository;
import com.persistence.bibliotheque.repository.LivreRepository;
import com.persistence.bibliotheque.service.LivreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/livres")
public class LivreController {

    private final LivreRepository livreRepository;
    private final EmpruntRepository empruntRepository;
    private final LivreService livreService;

    @Autowired
    public LivreController(LivreRepository livreRepository, EmpruntRepository empruntRepository, LivreService livreService) {
        this.livreRepository = livreRepository;
        this.empruntRepository = empruntRepository;
        this.livreService = livreService;
    }

    @PostMapping
    public ResponseEntity<LivreBo> createLivre(@RequestBody LivreBo livreBo) {
        LivreBo createdLivre = livreService.createLivre(livreBo);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdLivre);
    }

    @GetMapping("/{id}")
    public ResponseEntity<LivreBo> findLivreById(@PathVariable("id") Long id) {
        LivreBo livre = livreService.findLivreById(id);
        if (livre != null) {
            return ResponseEntity.ok(livre);
        }
        return ResponseEntity.notFound().build();
    }

    @PutMapping("/{id}/modifier")
    public ResponseEntity<LivreBo> updateLivre(@PathVariable("id") Long id, @RequestBody LivreBo livreBo) {
        livreBo.setId(id);
        LivreBo updatedLivre = livreService.updateLivre(livreBo);
        if (updatedLivre != null) {
            return ResponseEntity.ok(updatedLivre);
        }
        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/{id}/delete")
    public ResponseEntity<Void> deleteLivreById(@PathVariable("id") Long id) {
        Optional<Livre> optionalLivre = livreRepository.findById(id);
        if (optionalLivre.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        Livre livre = optionalLivre.get();

        List<Emprunt> emprunts = empruntRepository.findByLivre(livre);
        empruntRepository.deleteAll(emprunts);

        livreRepository.delete(livre);

        return ResponseEntity.noContent().build();
    }


    @GetMapping
    public ResponseEntity<List<LivreBo>> getAllLivres() {
        List<LivreBo> livres = livreRepository.findAllLivres();
        return ResponseEntity.ok(livres);
    }
}

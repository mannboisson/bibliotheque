package com.persistence.bibliotheque.controller;

import com.persistence.bibliotheque.bo.CategorieBo;
import com.persistence.bibliotheque.entity.Categorie;
import com.persistence.bibliotheque.entity.Livre;
import com.persistence.bibliotheque.repository.CategorieRepository;
import com.persistence.bibliotheque.repository.LivreRepository;
import com.persistence.bibliotheque.service.CategorieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/categories")
public class CategorieController {

    private final CategorieRepository categorieRepository;
    private final LivreRepository livreRepository;
    private final CategorieService categorieService;

    @Autowired
    public CategorieController(CategorieRepository categorieRepository, LivreRepository livreRepository, CategorieService categorieService) {
        this.categorieRepository = categorieRepository;
        this.livreRepository = livreRepository;
        this.categorieService = categorieService;
    }

    @PostMapping
    public ResponseEntity<CategorieBo> createCategorie(@RequestBody CategorieBo categorieBo) {
        CategorieBo createdCategorie = categorieService.createCategorie(categorieBo);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdCategorie);
    }

    @GetMapping("/{id}")
    public ResponseEntity<CategorieBo> findCategorieById(@PathVariable("id") Long id) {
        CategorieBo categorie = categorieService.findCategorieById(id);
        if (categorie != null) {
            return ResponseEntity.ok(categorie);
        }
        return ResponseEntity.notFound().build();
    }

    @PutMapping("/{id}/modifier")
    public ResponseEntity<CategorieBo> updateCategorie(@PathVariable("id") Long id, @RequestBody CategorieBo categorieBo) {
        categorieBo.setId(id);
        CategorieBo updatedCategorie = categorieService.updateCategorie(categorieBo);
        if (updatedCategorie != null) {
            return ResponseEntity.ok(updatedCategorie);
        }
        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/{id}/delete")
    public ResponseEntity<Void> deleteCategorieById(@PathVariable("id") Long id) {

        Optional<Categorie> optionalCategorie = categorieRepository.findById(id);
        if (optionalCategorie.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        Categorie categorie = optionalCategorie.get();


        List<Livre> livres = livreRepository.findByCategorie(categorie);
        for (Livre livre : livres) {
            livre.setCategorie(null);
            livreRepository.save(livre);
        }

        categorieRepository.delete(categorie);

        return ResponseEntity.noContent().build();
    }


    @GetMapping
    public ResponseEntity<List<CategorieBo>> getAllCategories() {
        List<CategorieBo> categories = categorieRepository.findAllCategories();
        return ResponseEntity.ok(categories);
    }
}

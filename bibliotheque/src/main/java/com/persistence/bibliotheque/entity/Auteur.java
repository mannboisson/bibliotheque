package com.persistence.bibliotheque.entity;

import jakarta.persistence.*;
import lombok.*;

import java.util.Date;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Auteur {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id ;

    @Column(name = "nom", nullable = false)
    private  String nom;

    @Column(name = "prenom", nullable = false)
    private Date prenom;
}

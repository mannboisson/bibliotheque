package com.persistence.bibliotheque.bo;

import com.persistence.bibliotheque.bo.AdherentBo;
import com.persistence.bibliotheque.bo.LivreBo;

import java.util.Date;

public class EmpruntBo {

    private Long id;
    private Date dateEmprunt;
    private Date dateFinPrevue;
    private Date dateRetour;
    private AdherentBo adherent;
    private LivreBo livre;

    public EmpruntBo(Long id, Date dateEmprunt, Date dateFinPrevue, Date dateRetour, AdherentBo adherent, LivreBo livre) {
        this.id = id;
        this.dateEmprunt = dateEmprunt;
        this.dateFinPrevue = dateFinPrevue;
        this.dateRetour = dateRetour;
        this.adherent = adherent;
        this.livre = livre;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDateEmprunt() {
        return dateEmprunt;
    }

    public void setDateEmprunt(Date dateEmprunt) {
        this.dateEmprunt = dateEmprunt;
    }

    public Date getDateFinPrevue() {
        return dateFinPrevue;
    }

    public void setDateFinPrevue(Date dateFinPrevue) {
        this.dateFinPrevue = dateFinPrevue;
    }

    public Date getDateRetour() {
        return dateRetour;
    }

    public void setDateRetour(Date dateRetour) {
        this.dateRetour = dateRetour;
    }

    public AdherentBo getAdherent() {
        return adherent;
    }

    public void setAdherent(AdherentBo adherent) {
        this.adherent = adherent;
    }

    public LivreBo getLivre() {
        return livre;
    }

    public void setLivre(LivreBo livre) {
        this.livre = livre;
    }
}

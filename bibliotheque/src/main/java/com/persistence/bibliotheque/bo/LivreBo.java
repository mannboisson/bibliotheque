package com.persistence.bibliotheque.bo;

import com.persistence.bibliotheque.entity.Auteur;
import com.persistence.bibliotheque.entity.Categorie;

import java.util.Date;

public class LivreBo {

    private Long id;
    private String titre;
    private Date dateDeParution;
    private int nombreDePages;
    private Auteur auteur;
    private Categorie categorie;

    public LivreBo(Long id, String titre, Date dateDeParution, int nombreDePages, Auteur auteur, Categorie categorie) {
        this.id = id;
        this.titre = titre;
        this.dateDeParution = dateDeParution;
        this.nombreDePages = nombreDePages;
        this.auteur = auteur;
        this.categorie = categorie;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public Date getDateDeParution() {
        return dateDeParution;
    }

    public void setDateDeParution(Date dateDeParution) {
        this.dateDeParution = dateDeParution;
    }

    public int getNombreDePages() {
        return nombreDePages;
    }

    public void setNombreDePages(int nombreDePages) {
        this.nombreDePages = nombreDePages;
    }

    public Auteur getAuteur() {
        return auteur;
    }

    public void setAuteur(Auteur auteur) {
        this.auteur = auteur;
    }

    public Categorie getCategorie() {
        return categorie;
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }



}

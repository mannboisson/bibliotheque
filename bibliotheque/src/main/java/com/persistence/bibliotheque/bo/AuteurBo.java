package com.persistence.bibliotheque.bo;

import java.util.Date;

public class AuteurBo {

    private Long id;
    private String nom;
    private Date prenom;

    public AuteurBo(Long id, String nom, Date prenom) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Date getPrenom() {
        return prenom;
    }

    public void setPrenom(Date prenom) {
        this.prenom = prenom;
    }
}

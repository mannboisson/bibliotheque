package com.persistence.bibliotheque.repository;

import com.persistence.bibliotheque.bo.AdherentBo;
import com.persistence.bibliotheque.entity.Adherent;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface AdherentRepository extends CrudRepository<Adherent, Long> {

    Adherent save(Adherent adherent);

    List<Adherent> findByIdArticle(long id);

    @Modifying
    @Transactional
    @Query(value = "UPDATE Adherent a SET a.nom = :nom, a.prenom = :prenom, a.email = :email WHERE a.id = :id")
    void updateAdherentWithId(@Param("id") long id, @Param("nom") String nom, @Param("prenom") String prenom, @Param("email") String email);

    List<Adherent> findAllAdherents();
}

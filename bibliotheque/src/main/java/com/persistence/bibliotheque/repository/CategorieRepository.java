package com.persistence.bibliotheque.repository;

import com.persistence.bibliotheque.bo.CategorieBo;
import com.persistence.bibliotheque.entity.Categorie;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategorieRepository extends CrudRepository<Categorie,Long > {
    List<CategorieBo> findAllCategories();
}

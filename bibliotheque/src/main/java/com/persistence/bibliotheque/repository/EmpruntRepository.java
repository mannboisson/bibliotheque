package com.persistence.bibliotheque.repository;

import com.persistence.bibliotheque.bo.EmpruntBo;
import com.persistence.bibliotheque.entity.Adherent;
import com.persistence.bibliotheque.entity.Emprunt;
import com.persistence.bibliotheque.entity.Livre;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmpruntRepository extends CrudRepository<Emprunt,Long > {
    List<EmpruntBo> findAllEmprunts();

    List<Emprunt> findByLivre(Livre livre);
}

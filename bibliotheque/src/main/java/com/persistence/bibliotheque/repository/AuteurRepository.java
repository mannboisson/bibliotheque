package com.persistence.bibliotheque.repository;

import com.persistence.bibliotheque.bo.AuteurBo;
import com.persistence.bibliotheque.entity.Adherent;
import com.persistence.bibliotheque.entity.Auteur;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AuteurRepository extends CrudRepository<Auteur,Long > {
    List<AuteurBo> findAllAuteurs();
}

package com.persistence.bibliotheque.repository;

import com.persistence.bibliotheque.bo.LivreBo;
import com.persistence.bibliotheque.entity.Adherent;
import com.persistence.bibliotheque.entity.Categorie;
import com.persistence.bibliotheque.entity.Livre;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LivreRepository extends CrudRepository<Livre,Long > {
    List<LivreBo> findAllLivres();

    List<Livre> findByCategorie(Categorie categorie);
}
